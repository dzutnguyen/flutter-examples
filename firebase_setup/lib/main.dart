import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'package:firebase_setup/model/board.dart';
import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';

final FirebaseAuth _auth = FirebaseAuth.instance;
// final GoogleSignIn _googleSignIn = GoogleSignIn();

GoogleSignIn _googleSignIn = GoogleSignIn(
  scopes: [
    'email',
    'https://www.googleapis.com/auth/contacts.readonly',
  ],
);

final FirebaseDatabase database = FirebaseDatabase.instance;

void main() => runApp(MyAppSignIn());

class MyAppSignIn extends StatefulWidget {
  @override
  _MyAppSignInState createState() => _MyAppSignInState();
}

class _MyAppSignInState extends State<MyAppSignIn> {
  GoogleSignInAccount _currentUser;
  String _imageURL;
  final _defaultImageURL =
      "https://lh3.googleusercontent.com/-MEfMkD9Mri0/AAAAAAAAAAI/AAAAAAAAAAA/ACHi3rfmk8umsTM_QylbiTZsCBqd7n70Eg/s48-c/photo.jpg";

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Board"),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.network(_imageURL == null || _imageURL.isEmpty
                  ? _defaultImageURL
                  : _imageURL),
              FlatButton(
                child: Text("Google-Signin"),
                onPressed: _signIn,
                color: Colors.red,
              ),
              FlatButton(
                child: Text("Sign in with email"),
                onPressed: _signInWithEmail,
                color: Colors.orange,
              ),
              FlatButton(
                child: Text("Create Account"),
                onPressed: () => _createUser(),
                color: Colors.purple,
              ),
              FlatButton(
                child: Text("Sign out"),
                onPressed: () {
                  _logout();
                },
                color: Colors.orange[300],
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    _googleSignIn.onCurrentUserChanged.listen((GoogleSignInAccount account) {
      setState(() {
        _currentUser = account;
      });
      if (_currentUser != null) {
        // _handleGetContact();
      }
    });
    _googleSignIn.signInSilently();
  }

  Future<FirebaseUser> _signIn() async {
    _currentUser = await _googleSignIn.signIn();
    GoogleSignInAuthentication googleSignInAuthentication =
        await _currentUser.authentication;

    final AuthCredential credential = GoogleAuthProvider.getCredential(
      accessToken: googleSignInAuthentication.accessToken,
      idToken: googleSignInAuthentication.idToken,
    );

    final AuthResult authResult = await _auth.signInWithCredential(credential);
    final FirebaseUser user = authResult.user;
    print("User: ${user.displayName}");

    setState(() {
      _imageURL = user.photoUrl;
    });

    return user;
  }

  _createUser() async {
    AuthResult authResult = await _auth
        .createUserWithEmailAndPassword(
      email: "yumeyumeyuyuyu1@gmail.com",
      password: "It's a lot of 1234",
    )
        .then((auth) {
      print("User: ${auth.user.displayName}");
      print("User: ${auth.user.email}");
    }).catchError((e) {
      print(e);
    });
  }

  void _logout() {
    _googleSignIn.signOut();
    setState(() {
      _imageURL = null;
    });
  }

  void _signInWithEmail() {
    _auth
        .signInWithEmailAndPassword(
      email: "yumeyumeyuyuyu1@gmail.com",
      password: "It's a lot of 1234",
    )
        .then((auth) {
      print("User signed in: ${auth.user.email.toString()}");
    }).catchError((error) {
      print(error.toString());
    });
  }
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  var boardMessages = <Board>[];
  Board board;
  final FirebaseDatabase database = FirebaseDatabase.instance;
  final GlobalKey<FormState> formKey =
      GlobalKey<FormState>(); //This is used to identify the form
  DatabaseReference databaseReference;

  // void _incrementCounter() {
  //   database
  //       .reference()
  //       .child("message")
  //       .set({"firstname": "Ngan", "lastname": "Dien", "age": 29});
  //   setState(() {
  //     database
  //         .reference()
  //         .child("message")
  //         .once()
  //         .then((DataSnapshot snapshot) {
  //       Map<dynamic, dynamic> data = snapshot.value;
  //       print("Values from db:  ${data.values}");
  //     });

  //     _counter++;
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Firebase Setup"),
      ),
      body: Column(
        children: <Widget>[
          Flexible(
            flex: 0,
            child: Form(
              key: formKey, // assign the form key so we can identify the form
              child: Flex(
                direction: Axis.vertical,
                children: <Widget>[
                  ListTile(
                    leading: Icon(Icons.subject),
                    title: TextFormField(
                      initialValue: "",
                      onSaved: (value) {
                        board.subject = value;
                      },
                      validator: (value) {
                        return value == "" ? value : null;
                      },
                    ),
                  ),
                  ListTile(
                    leading: Icon(Icons.message),
                    title: TextFormField(
                      initialValue: "",
                      onSaved: (value) {
                        board.body = value;
                      },
                      validator: (value) {
                        return value == "" ? value : null;
                      },
                    ),
                  ),
                  FlatButton(
                    child: Text("Post"),
                    onPressed: _handleSubmit,
                    color: Colors.redAccent,
                  ),
                ],
              ),
            ),
          ),
          Flexible(
            child: FirebaseAnimatedList(
              query: databaseReference,
              itemBuilder: (_, DataSnapshot snapshot,
                  Animation<double> animation, int index) {
                return Card(
                  child: ListTile(
                    leading: CircleAvatar(
                      backgroundColor: Colors.red,
                    ),
                    title: Text(boardMessages[index].subject),
                    subtitle: Text(boardMessages[index].body),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    board = Board("", "");
    databaseReference = database.reference().child("community_board");
    databaseReference.onChildAdded.listen(_onEntryAdded);
    databaseReference.onChildChanged.listen(_onEntryChanged);
  }

  void _onEntryAdded(Event event) {
    setState(() {
      boardMessages.add(Board.fromSnapshot(event.snapshot));
    });
  }

  void _handleSubmit() {
    final FormState form = formKey.currentState;
    if (form.validate()) {
      form.save();
      form.reset();

      //save form data to database
      // push() creates a new child location with a unique key
      databaseReference.push().set(board.toJson());
    }
  }

  void _onEntryChanged(Event event) {
    var oldEntry = boardMessages.singleWhere((entry) {
      return entry.key == event.snapshot.key;
    });
    setState(() {
      boardMessages[boardMessages.indexOf(oldEntry)] =
          Board.fromSnapshot(event.snapshot);
    });
  }
}
