import 'package:flutter/material.dart';
import 'package:provider_counter_app/bloc/counter_bloc.dart';
import 'package:provider/provider.dart';

class Name {}

Widget counterValue(BuildContext context) {
  CounterBloc bloc = Provider.of<CounterBloc>(context);
  return Container(
    width: 200,
    height: 300,
    padding: EdgeInsets.all(20.0),
    alignment: Alignment.center,
    decoration: BoxDecoration(
      color: Colors.green,
      borderRadius: BorderRadius.circular(50),
    ),
    child: Text(
      bloc.counter.toString(),
      style: TextStyle(
        fontSize: 80.0,
      ),
    ),
  );
}
