import 'package:flutter/material.dart';

Widget counterButtons(BuildContext context) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: <Widget>[
      FloatingActionButton(
        onPressed: () {},
        child: Icon(Icons.add),
        tooltip: 'Increment',
      ),
      SizedBox(
        width: 40.0,
      ),
      FloatingActionButton(
        onPressed: () {},
        child: Icon(Icons.remove),
        tooltip: 'Decrement',
      ),
    ],
  );
}
