import 'package:flutter/foundation.dart';

class CounterBloc extends ChangeNotifier {
  int _counter = 0;
  void increment() {
    _counter++;
  }

  void decrement() {
    _counter--;
  }

  int get counter => _counter;
}
