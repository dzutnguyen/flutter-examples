
import 'package:flutter/material.dart';
import 'package:provider_counter_app/widgets/counter_buttons.dart';
import 'package:provider_counter_app/widgets/counter_value.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Provider Counter App'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            counterValue(context),
            SizedBox(
              height: 20.0,
            ),
            counterButtons(context),
          ],
        ),
      ),
    );
  }
}
