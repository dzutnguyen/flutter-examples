import 'package:flutter/material.dart';
import 'package:provider_counter_app/bloc/counter_bloc.dart';
import 'screens/home_page.dart';
import 'package:provider/provider.dart';

void main() => runApp(App());

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(primarySwatch: Colors.red),
      home: ChangeNotifierProvider<CounterBloc>.value(
        child: HomePage(),
        value: CounterBloc(),
      ),
    );
  }
}
