import 'package:flutter/material.dart';

void main() => runApp(MaterialApp(
      home: Scaffold(
        body: MyApp(),
      ),
    ));

class MyApp extends StatelessWidget {
  String descriptionText = "Lake Oeschinen lies at the foot of the Bluemlisalp in the bernese Alps. Situated 1578 meters above the sea level, it is one of the larger Alpine Lakes. A gondola ride from kandersteg, floowed by a half-hour walk through  pastures and pine forest, leads you to the lake, which warms to 20 degrees Celsius in the summer. Activities enjoyed here include rowing, and riding the summer toboggan run";

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          // image
          Image.asset("images/lake.jpg"),
          // Title and subtitle
          TitleName(),

          // contact
          Options(),

          // detail
          Padding(
            padding: const EdgeInsets.all(32.0),
            child: Text(descriptionText),
          ),
        ],
      ),
    );
  }
}

Widget getIconOption(IconData data, String name) {
  return Column(
    children: <Widget>[
      Icon(
        data,
        color: Colors.blueAccent,
      ),
      Text(
        name,
        style: TextStyle(color: Colors.blueAccent),
      ),
    ],
  );
}

class Options extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Expanded(child: getIconOption(Icons.call, "CALL")),
        Expanded(child: getIconOption(Icons.navigation, "ROUTE")),
        Expanded(child: getIconOption(Icons.share, "SHARE")),
      ],
    );
  }
}

class TitleName extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(32.0),
      child: Row(
        children: <Widget>[
          // title and subtitle
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  "Oeschinen Lake Campground",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  "Kandersteg, Switzerland",
                  style: TextStyle(
                    color: Colors.black45,
                  ),
                ),
              ],
            ),
          ),

          // rating
          Row(
            children: <Widget>[
              Icon(
                Icons.star,
                color: Colors.redAccent,
              ),
              Text("41"),
            ],
          ),
        ],
      ),
    );
  }
}
